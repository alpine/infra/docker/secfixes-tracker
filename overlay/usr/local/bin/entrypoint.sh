#!/bin/sh

# shellcheck disable=SC3040
set -eu -o pipefail

if ! [ -f /home/python/db/secfixes-tracker.sqlite ]; then
    echo "-> Initializing the database"
    flask init-db
fi

# https://github.com/unbit/uwsgi/issues/2299
# shellcheck disable=SC3045
ulimit -n 2048

# exec flask run --host 0.0.0.0
exec uwsgi --master \
    --plugin python3 --manage-script-name --mount /=secfixes-tracker:app \
    --plugin http --http 0.0.0.0:8080 \
    --processes=4 --threads=2
