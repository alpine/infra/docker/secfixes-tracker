# Secfixes settings

# repo -> uri
SECFIXES_REPOSITORIES = {
}

# repo -> uri
APKINDEX_REPOSITORIES = {
}

# Database settings
SQLALCHEMY_DATABASE_URI="sqlite:////home/python/db/secfixes-tracer.sqlite"
SQLALCHEMY_TRACK_MODIFICATIONS=False
