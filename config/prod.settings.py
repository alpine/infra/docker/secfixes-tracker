# Database settings
SQLALCHEMY_DATABASE_URI = "sqlite:////home/python/db/secfixes-tracker.sqlite"
SQLALCHEMY_TRACK_MODIFICATIONS = False

# repo -> uri
SECFIXES_REPOSITORIES = {
    "edge-main": "https://secdb.alpinelinux.org/edge/main.json",
    "edge-community": "https://secdb.alpinelinux.org/edge/community.json",
    "3.21-main": "https://secdb.alpinelinux.org/v3.21/main.json",
    "3.21-community": "https://secdb.alpinelinux.org/v3.21/community.json",
    "3.20-main": "https://secdb.alpinelinux.org/v3.20/main.json",
    "3.19-main": "https://secdb.alpinelinux.org/v3.19/main.json",
    "3.18-main": "https://secdb.alpinelinux.org/v3.18/main.json",
}

# repo -> uri
APKINDEX_REPOSITORIES = {
    "edge-main": "https://dl-cdn.alpinelinux.org/alpine/edge/main/x86_64/APKINDEX.tar.gz",
    "edge-community": "https://dl-cdn.alpinelinux.org/alpine/edge/community/x86_64/APKINDEX.tar.gz",
    "3.21-main": "https://dl-cdn.alpinelinux.org/alpine/v3.21/main/x86_64/APKINDEX.tar.gz",
    "3.21-community": "https://dl-cdn.alpinelinux.org/alpine/v3.21/community/x86_64/APKINDEX.tar.gz",
    "3.20-main": "https://dl-cdn.alpinelinux.org/alpine/v3.20/main/x86_64/APKINDEX.tar.gz",
    "3.19-main": "https://dl-cdn.alpinelinux.org/alpine/v3.19/main/x86_64/APKINDEX.tar.gz",
    "3.18-main": "https://dl-cdn.alpinelinux.org/alpine/v3.18/main/x86_64/APKINDEX.tar.gz",
}

# repo -> uri
SECURITY_REJECTIONS = {
    "edge-main": "https://gitlab.alpinelinux.org/Ariadne/security-rejections/-/raw/master/main.yaml",
    "edge-community": "https://gitlab.alpinelinux.org/Ariadne/security-rejections/-/raw/master/community.yaml",
    "3.21-main": "https://gitlab.alpinelinux.org/Ariadne/security-rejections/-/raw/master/main.yaml",
    "3.21-community": "https://gitlab.alpinelinux.org/Ariadne/security-rejections/-/raw/master/community.yaml",
    "3.20-main": "https://gitlab.alpinelinux.org/Ariadne/security-rejections/-/raw/master/main.yaml",
    "3.19-main": "https://gitlab.alpinelinux.org/Ariadne/security-rejections/-/raw/master/main.yaml",
    "3.18-main": "https://gitlab.alpinelinux.org/Ariadne/security-rejections/-/raw/master/main.yaml",
}

PACKAGE_EXCLUSIONS = [
    "alpine-base",
    "build-base",
    "docs",
]

CUSTOM_REWRITERS = {
    "jenkins:*": lambda x: "jenkins",
    "golang:protobuf": lambda x: "go-protobuf",
    "boltcms:bolt": lambda x: "boltcms",
    "samsung:*": lambda x: "samsung-" + x,
    "cygwin:*": lambda x: "cygwin-" + x,
    "facebook:zstandard": lambda x: "zstd",
    "nodejs:*": lambda x: "nodejs",
    "mozilla:network_security_services": lambda x: "nss",
    "brokenlamp:slock": lambda x: "rust-slock",
    "knplabs:snappy": lambda x: "knplabs-snappy",
    "home-assistant:supervisor": lambda x: "homeassistant-supervisor",
    "edwiser:bridge": lambda x: "wordpress-edwiser-bridge",
    "adobe:bridge": lambda x: "adobe-bridge",
}
