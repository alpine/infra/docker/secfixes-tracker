ARG ALPINE_VERSION=3.20
FROM curlimages/curl AS source

ARG SECFIXES_TRACKER_VERSION

RUN <<EOF
set -e
mkdir -p secfixes-tracker
curl -vf -O https://gitlab.alpinelinux.org/alpine/security/secfixes-tracker/-/archive/$SECFIXES_TRACKER_VERSION/secfixes-tracker-$SECFIXES_TRACKER_VERSION.tar.gz
tar xzf secfixes-tracker-$SECFIXES_TRACKER_VERSION.tar.gz --strip-components=1 --directory secfixes-tracker
EOF

ARG ALPINE_VERSION
FROM alpinelinux/golang:$ALPINE_VERSION as builder

COPY --from=source --chown=build:build /home/curl_user/secfixes-tracker /home/build/secfixes-tracker

WORKDIR /home/build/secfixes-tracker

RUN redo

ARG ALPINE_VERSION
FROM alpine:$ALPINE_VERSION

COPY overlay /

RUN apk add \
    uwsgi \
    uwsgi-python3 \
    uwsgi-http \
    py3-greenlet \
    py3-pip \
    py3-requests \
    py3-sqlalchemy \
    py3-yaml \
    curl \
    patch \
    && adduser -D python \
    && install -d -opython -gpython /home/python/secfixes-tracker \
    && install -d -opython -gpython /home/python/db

USER python
WORKDIR /home/python/secfixes-tracker

COPY --from=builder --chmod=0755 /home/build/secfixes-tracker/secfixes-cli /usr/local/bin/
COPY --from=source --chown=python:python /home/curl_user/secfixes-tracker /home/python/secfixes-tracker
RUN <<EOF
set -e
pip install --break-system-packages -r requirements.txt
EOF

ENV PATH="${PATH}:/home/python/.local/bin"

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
